
# 1
name = "Jose"
age = 38
occupation = "writer"
movie = "One More Chance"
rating = 99.6

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%"
)



# 2
num1,num2,num3 = 1,2,3
# product of num1 and num2
print(num1*num2)

# check if less than
print(num1<num3)

# add the value of num3 to num2
print(num3+num2)